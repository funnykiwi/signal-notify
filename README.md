# Signal-Notify

A background service that can be connected to [Signal](https://signal.org/) and will show a desktop notification on message receive.

## Usage

Compile the application (assuming you have [Rust](https://www.rust-lang.org/) installed):

```
cargo build --release
```

The binary will be located in `target/release/signal_notify`. Optionally, move it into `$PATH`. Execute the script. On the first execution, a QR-code will be shown that can be used to link the device to the official Signal application on your phone. Scan it and grant access to the application.

Now, everything should work fine. You can test it by for example sending a "Note to self" in the Signal application, your note should pop up as a notification.

## Auto-start

In the repository, the `signal-notify@alarm.service` can be used to auto-start the daemon using systemd. 

To use it, replace `alarm` in the filename with your username, this will specify what directory to use, copy the file to `/usr/lib/systemd/system` (on Arch Linux and derivatives, might vary depending on the distribution) and start the service using `sudo systemctl enable --now signal-notify@alarm` (again, replacing `alarm` with your user). Note that this service assumes your binary to be somewhere in the `$PATH`. Also make sure to run the binary at least once normally so you can link it. 

## Security

This application does, to the best of my knowledge, neither store messages on the disk nor keep them in memory longer than needed to send the notification. Regardless, this application will likely worsen the security compared to official Signal products. Use this application with care when handling sensitive data.
