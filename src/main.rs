use std::collections::HashMap;
use std::process::Command;
use std::sync::{Arc, Mutex};
use std::time::Duration;
use std::{env, path::PathBuf};

use futures::{pin_mut, StreamExt};
use presage::prelude::content::Reaction;
use presage::prelude::proto::call_message::offer::Type;
use presage::prelude::proto::sync_message::Sent;
use presage::prelude::proto::CallMessage;
use presage::{prelude::*, Error, Manager, SledConfigStore};

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    env_logger::init();

    log::trace!("Starting up signal");

    let db_path = PathBuf::from(
        env::var("SN_CONFIG_PATH").unwrap_or(
            env::var("XDG_CONFIG_HOME")
                .map(|s| s + "/signal_notify/")
                .unwrap_or(env::var("HOME").map(|s| s + "/.config/signal_notify/")?),
        ),
    );
    log::debug!("Use configuration directory: {}", db_path.display());
    let config_store = SledConfigStore::new(db_path)?;

    log::debug!("Setting up signal manager");
    let csprng = rand::thread_rng();
    let mut manager = Manager::new(config_store, csprng)?;

    if !manager.is_registered() {
        log::debug!("Currently not linked, linking");
        manager
            .link_secondary_device(SignalServers::Production, "signal_notify".to_string())
            .await?;
        log::trace!("Finished linking");
    }

    log::debug!("Synchronizing contacts.");
    manager.request_contacts_sync().await?;
    log::trace!("Synchronizing contacts finished.");

    let read_stamps: HashMap<String, u64> = HashMap::new();

    let read_stamps_mutex = Arc::new(Mutex::new(read_stamps));

    loop {
        log::debug!("Querying messages");
        let messages = manager.receive_messages().await?;
        pin_mut!(messages);
        log::trace!("Querying messages finished");

        while let Some(Content { metadata, body }) = messages.next().await {
            log::debug!("Got a message. Checking what to do with it");
            match body {
                ContentBody::CallMessage(CallMessage {
                    offer: Some(offer), ..
                }) => {
                    let body = match offer.r#type() {
                        Type::OfferAudioCall => "is audio calling",
                        Type::OfferVideoCall => "is video calling",
                    };

                    let sender_contact = metadata
                        .sender
                        .uuid
                        .map(|uuid| manager.get_contact_by_id(uuid));
                    let my_uuid = manager.uuid();
                    show_notification(metadata.sender, sender_contact, my_uuid, body)?;
                }
                ContentBody::DataMessage(message)
                | ContentBody::SynchronizeMessage(SyncMessage {
                    sent:
                        Some(Sent {
                            message: Some(message),
                            ..
                        }),
                    ..
                }) => {
                    if message.delete.is_some() {
                        // skip delete messages
                        continue;
                    }

                    if let Some(Reaction {
                        remove: Some(true), ..
                    }) = &message.reaction
                    {
                        // skip emoji reaction removals
                        continue;
                    }

                    log::debug!("Got actual message. Building notification");

                    let mut attachment = "";
                    for a in &message.attachments {
                        match &a.content_type {
                            Some(content_type) => {
                                let media_type = &content_type[..6];
                                attachment = match media_type {
                                    "audio/" => "Audio, ",
                                    "image/" => "Photo, ",
                                    "video/" => "Video, ",
                                    _ => continue,
                                };
                                break;
                            }
                            None => continue,
                        };
                    }

                    let reaction = if let Some(Reaction {
                        emoji: Some(emoji), ..
                    }) = message.reaction
                    {
                        format!("Reacted {}", emoji)
                    } else {
                        String::new()
                    };

                    let sticker_found = message.sticker.is_some();

                    let text_body = message.body.unwrap_or_else(|| "No body.".to_string());
                    let body = if sticker_found {
                        format!("Sticker, {}", text_body)
                    } else if !reaction.is_empty() {
                        reaction
                    } else {
                        format!("{}{}", attachment, text_body)
                    };

                    log::trace!("Body: {}", body);

                    let c_mutex = Arc::clone(&read_stamps_mutex);

                    let sender_contact = metadata
                        .sender
                        .uuid
                        .map(|uuid| manager.get_contact_by_id(uuid));
                    let my_uuid = manager.uuid();

                    tokio::spawn(async move {
                        tokio::time::sleep(Duration::from_secs(1)).await;

                        let c_mutex_owned = c_mutex.to_owned();
                        let read_map = c_mutex_owned.lock().unwrap();

                        if let Some(last_read_ts) =
                            read_map.get(&metadata.sender.uuid.unwrap().to_string())
                        {
                            if &message.timestamp.unwrap() <= last_read_ts {
                                // skip already read messages
                                return;
                            }
                        }

                        show_notification(metadata.sender, sender_contact, my_uuid, &body).unwrap();
                    });
                }
                ContentBody::SynchronizeMessage(SyncMessage { read: reads, .. }) => {
                    let mut read_stamps_mg = read_stamps_mutex.lock().unwrap();
                    for read in &reads {
                        read_stamps_mg.insert(
                            read.sender_uuid.as_ref().unwrap().to_string(),
                            read.timestamp.unwrap(),
                        );
                    }
                }
                _ => {
                    log::debug!("This message cannot currently be handled.");
                }
            }
        }
        log::info!("Messages channel disconnected. Trying to reconnect.");
    }
}

fn show_notification(
    sender: ServiceAddress,
    sender_contact: Option<Result<Option<Contact>, Error>>,
    my_uuid: Uuid,
    body: &str,
) -> Result<(), Box<dyn std::error::Error>> {
    let sender_name = if let Some(uuid) = sender.uuid {
        if let Some(Ok(Some(c))) = sender_contact {
            if my_uuid == uuid {
                // skip own messages
                return Ok(());
            }
            if c.name.is_empty() {
                sender
                    .phonenumber
                    .expect("sender phone number to be set")
                    .to_string()
            } else {
                c.name
            }
        } else {
            "Unknown sender".to_string()
        }
    } else {
        "Unknown sender".to_string()
    };
    log::trace!("Sender Name: {}", sender_name);

    Command::new("ffplay")
        .arg("-autoexit")
        .arg("-showmode")
        .arg("0")
        .arg("./notification_sound.mp3")
        .output()
        .expect("failed to execute process");

    Command::new("notify-send")
        .arg(sender_name)
        .arg(body)
        .arg("-i")
        .arg("signal-desktop")
        .arg("-t")
        .arg("0")
        .output()
        .expect("failed to execute process");

    Ok(())
}
